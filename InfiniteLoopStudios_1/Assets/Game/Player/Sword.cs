using UnityEngine;

public class Sword : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<SkeletonController>().OnBeingHit(1);
    }
}
