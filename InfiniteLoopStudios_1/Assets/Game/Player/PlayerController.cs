using UnityEngine;

// Player controller is a state machine
public class PlayerController : MonoBehaviour
{
    enum PlayerState
    {
        move,
        attack,
    }
    [Tooltip("Running speed")]
    public float speed = 5f;
    [Tooltip("Initial vertical speed when jumping")]
    public float jumpTakeOffSpeed = 7f;
    [Tooltip("Gravity constant")]
    public float gravity = 9.8f;
    [Tooltip("Layer mask for grounding detection")]
    public LayerMask ground;
    [Tooltip("Grounding detection sphere radius")]
    public float groundDetectorRadius = .8f;
    [Tooltip("Grounding detection sphere radius")]
    public Vector3 groundDetectorOffset = Vector3.zero;
    [Tooltip("Assign the weapon hit box here")]
    public BoxCollider hitbox;
    [Tooltip("Assign the footstep audio clip here")]
    public AudioClip footsteps;
    [Tooltip("Assign the weapon swing audio clip here")]
    public AudioClip swing;
    [Tooltip("Assign the jump audio clip here")]
    public AudioClip jump;
    // Grounding flag
    bool isGrounded = false;
    // Horizontal speed
    float horizontalSpeed = 0f;
    // Vertical speed
    float verticalSpeed = 0f;
    // flag that signals state transition by the end of attack animation
    bool attackComplete;
    // state machine current state 
    PlayerState state = PlayerState.move;
    // Character controller handle
    CharacterController controller;
    // Animator handle
    Animator animator;
    // Audio source handle
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update state machine current state
    void Update()
    {
        switch (state)
        {
            case PlayerState.move:
                UpdateMoveState();
                break;
            case PlayerState.attack:
                UpdateAttackState();
                break;
        }
    }

    // Handles movement input and actuation including moving left, right and jump, it will transition into attack state on recieving attack input
    void UpdateMoveState()
    {
        // transition into attack state on recieving attack input 
        if (Input.GetKeyDown(KeyCode.J))
        {
            Attack();
            return;
        }

        // fix a bug where jump trigger won't reset before landing 
        if (verticalSpeed < 0)
        {
            animator.ResetTrigger("Jump");
        }

        // grounding detection and gravity update
        Vector3 moveVector = Vector3.zero;
        bool isGroundPreviousFrame = isGrounded;
        isGrounded = Physics.CheckSphere(transform.position + groundDetectorOffset, groundDetectorRadius, ground, QueryTriggerInteraction.Ignore);
        if (isGroundPreviousFrame != isGrounded)
        {
            PlayFootSteps();
        }
        if (isGrounded)
        {
            verticalSpeed = 0;
            animator.SetBool("isGrounded", true);
        }
        else
        {
            verticalSpeed -= gravity * Time.deltaTime;
            animator.SetBool("isGrounded", false);
        }

        //Horizontal movement
        float moveDirection = Input.GetAxis("Horizontal");
        if (moveDirection != 0)
        {
            transform.forward = moveDirection * Vector3.right;
        }
        horizontalSpeed = speed * moveDirection;

        //Vertical movement
        if (Input.GetKey(KeyCode.Space))
        {
            Jump();
        }

        //motion actuation
        moveVector.x = horizontalSpeed * Time.deltaTime;
        moveVector.y = verticalSpeed * Time.deltaTime;
        moveVector.z = 0f;
        controller.Move(moveVector);
        animator.SetFloat("Speed", Mathf.Abs(moveDirection));

    }

    //Signal the animation for state transition upon releasing the button, the animation event will execute the actual state transition
    void UpdateAttackState()
    {
        if (Input.GetKeyUp(KeyCode.J))
        {
            attackComplete = true;
        }
    }

    //Handles jumping
    void Jump()
    {
        if (isGrounded)
        {
            verticalSpeed += jumpTakeOffSpeed;
            animator.SetTrigger("Jump");
            isGrounded = false;
            audioSource.PlayOneShot(jump);
        }
    }

    //Transition to attack state
    void Attack()
    {
        if (isGrounded)
        {
            animator.SetBool("isAttacking",true);
            state = PlayerState.attack;
        }
    }

    // this function is called by an event in the attack animation, it transition back to move state
    void AttackComplete()
    {
        if (attackComplete)
        {
            animator.SetBool("isAttacking", false);
            state = PlayerState.move;
            attackComplete = false;
        }
    }

    // this function is called by an event in the attack animation, it enables the hitbox
    void EnableHitBox()
    {
        hitbox.enabled = true;
    }

    // this function is called by an event in the attack animation, it disables the hitbox
    void DisableHitBox()
    {
        hitbox.enabled = false;
    }

    //this function is called by events in the run animation, it plays the footstep sound
    void PlayFootSteps()
    {
        audioSource.PlayOneShot(footsteps);
    }

    //this function is called by an event in the attack animation, it plays the weapon footstep sound
    void PlaySwingSound()
    {
        audioSource.PlayOneShot(swing);
    }

    // displays the grounding detector in the editor
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position + groundDetectorOffset, groundDetectorRadius);
    }
}
