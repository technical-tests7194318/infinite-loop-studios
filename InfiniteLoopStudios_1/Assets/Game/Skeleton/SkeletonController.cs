using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

// Skeleton controller is a state machine
public class SkeletonController : MonoBehaviour
{
    enum SkeletonState
    {
        idle,
        patrol,
        aggressed,
        attack,
        stagger,
        death
    }

    [Tooltip("Walking speed during patrol state")]
    public float speed = 1.0f;
    [Tooltip("Running speed during aggressed state")]
    public float runSpeed = 2.0f;
    [Tooltip("Health")]
    public float health = 4.0f;
    [Tooltip("Minimal idle time upon reach a patrol waypoint")]
    public float minIdletime = 1.0f;
    [Tooltip("Maximum idle time upon reach a patrol waypoint")]
    public float maxIdleTime = 2.0f;
    [Tooltip("Attack range")]
    public float attackRange = 1.5f;
    [Tooltip("Attack interval")]
    public float attackInterval = 1.0f;
    [Tooltip("A small offset to ensure the character is solid within attack range when approaching the player")]
    public float approachThreshold = 0.1f;
    [Tooltip("Stagger time")]
    public float staggerTime = 0.4f;
    [Tooltip("Stagger push back distance")]
    public float maxStaggerDistance = 2.0f;
    [Tooltip("Stagger push back speed")]
    public float staggerSpeed = 1.0f;
    [Tooltip("Assign footstep audio clip here")]
    public AudioClip footSteps;
    [Tooltip("Assign audio clip of BEING hit here")]
    public AudioClip hit;
    [Tooltip("Assign weapon swing audio clip here")]
    public AudioClip swing;
    [Tooltip("Assign skeleton collapse audio clip here")]
    public AudioClip collapse;
    [Tooltip("Assign patrol waypointshere")]
    public List<Transform> patrolWaypoint;
    [Tooltip("Assign enable \"You Won\" message UI event here")]
    public UnityEvent OnDeath;

    //timer that keeps track of idle time
    float idleTimer;
    //timer that keeps track of attack interval
    float attackTimer;
    //current waypoint index
    int waypointCounter;
    //target stagger pushback position
    Vector3 staggerPosition;
    //timer that keeps track of stagger time
    float staggerTimer;
    //player tracker
    Transform player;
    // skeleton current state
    SkeletonState state = SkeletonState.idle;
    // Animator handle
    Animator animator;
    // NavMeshAgent handle
    NavMeshAgent navMeshAgent;
    // AudioSource handle
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case SkeletonState.idle:
                UpdateIdleState();
                break;
            case SkeletonState.patrol:
                UpdatePatrolState();
                break;
            case SkeletonState.aggressed:
                UpdateAggressedState();
                break;
            case SkeletonState.attack:
                UpdateAttackState();
                break;
            case SkeletonState.stagger:
                UpdateStaggerState();
                break;
            case SkeletonState.death:
                break;
        }
    }

    //skeleton stands still, it will transition to patrol state upon time out or transiton to aggressed state upon detecting player
    void UpdateIdleState()
    {
        if (player)
        {
            TransitionToAggressed();
            return;
        }
        idleTimer -= Time.deltaTime;
        if (idleTimer <= 0)
        {
            TransitionToPatrol();
        }
    }
    //skeleton approaches next waypoint, it will transition to idle state upon reaching waypoint or transiton to aggressed state upon detecting player
    void UpdatePatrolState()
    {
        if (player)
        {
            TransitionToAggressed();
            return;
        }
        Vector3 currentTarget = patrolWaypoint[waypointCounter].position;
        navMeshAgent.SetDestination(currentTarget);
        //code written by copilot destination reached
        if (!navMeshAgent.pathPending)
        {
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    TransitionToIdle();
                }
            }
        }
    }
    //skeleton chases after the player, it transitions to attack state upon reaching the player or return to idle if the player's no longer detected
    void UpdateAggressedState()
    {
        if (!player)
        {
            TransitionToIdle();
            return;
        }
        float targetDistance = (player.position - transform.position).magnitude;
        if (targetDistance <= attackRange)
        {
            TransitionToAttack();
        }
    }
    //skeleton attacks the player, it transitions to aggressed state after player leaves the attack range or return to idle if the player's no longer detected
    void UpdateAttackState()
    {
        if (!player)
        {
            TransitionToIdle();
            return;
        }
        attackTimer += Time.deltaTime;
        if (attackTimer >= attackInterval)
        {
            animator.SetTrigger("Attack");
            attackTimer = 0;
        }
        transform.forward = player.position - transform.position;
        float targetDistance = transform.forward.magnitude;
        if (targetDistance > attackRange)
        {
            TransitionToAggressed();
        }
    }
    //skeleton is being hit and pushed back, recover to aggressed state once the stagger times out
    void UpdateStaggerState()
    {
        if (!player)
        {
            navMeshAgent.enabled = true;
            navMeshAgent.isStopped = false;
            TransitionToIdle();
            return;
        }
        staggerTimer += Time.deltaTime;
        Vector3 moveVector = (transform.position - player.position).normalized;
        transform.position = Vector3.Lerp(transform.position, staggerPosition, staggerSpeed * Time.deltaTime);
        if (staggerTimer >= staggerTime)
        {
            if (health <= 0)
            {
                state = SkeletonState.death;
                this.enabled = false;
            }
            else
            {
                navMeshAgent.enabled = true;
                navMeshAgent.isStopped = false;
                TransitionToAggressed();
            }
        }
    }

    // called by the hit box up on being hit
    public void OnBeingHit(float damage)
    {
        health -= damage;
        TransitionToStagger();
        audioSource.PlayOneShot(hit);
        if (health <= 0)
        {
            animator.SetTrigger("Killed");
            GetComponent<Collider>().enabled = false;
        }
    }
    // called by an event on the death animation
    void InvokeWinEvent()
    {
        OnDeath.Invoke();
        Time.timeScale = 0f;
    }
    
    void TransitionToIdle()
    {
        idleTimer = Random.Range(minIdletime, maxIdleTime);
        animator.SetFloat("Speed", 0.0f);
        state = SkeletonState.idle;
    }
    void TransitionToPatrol()
    {
        int previousWaypoint = waypointCounter;
        waypointCounter = Random.Range(0, 3);
        if (previousWaypoint == waypointCounter)
        {
            waypointCounter = (previousWaypoint + 1) % 3;
        }
        animator.SetFloat("Speed", 0.6f);
        state = SkeletonState.patrol;
    }
    void TransitionToAggressed()
    {
        navMeshAgent.SetDestination(player.position + (transform.position - player.position).normalized * (attackRange - approachThreshold));
        animator.SetFloat("Speed", 1.0f);
        state = SkeletonState.aggressed;
    }
    void TransitionToAttack()
    {
        attackTimer = attackInterval;
        state = SkeletonState.attack;
    }
    void TransitionToStagger()
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
        state = SkeletonState.stagger;
        animator.SetFloat("Speed", 0f);
        //need to disable pause and disable navmesh agent or the push back wont work
        navMeshAgent.isStopped = true;
        navMeshAgent.enabled = false;
        Vector3 targetVector = player.position - transform.position;
        staggerPosition = transform.position - (targetVector.x * Vector3.right).normalized * (maxStaggerDistance / targetVector.magnitude);
        staggerTimer = 0;
    }

    //playes footstep sound, called by running anmation event
    void PlayFootSteps()
    {
        audioSource.PlayOneShot(footSteps);
    }
    //playes weapon swing sound, called by attack anmation event
    void PlaySwingSound()
    {
        audioSource.PlayOneShot(swing);
    }
    //playes collapes sound, called by death anmation event
    void PlayCollapseSound()
    {
        audioSource.PlayOneShot(collapse);
    }

    //detects player
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            player = collision.transform;
        }
    }

    //undetects player
    private void OnTriggerExit(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            player = null;
        }
    }
}


