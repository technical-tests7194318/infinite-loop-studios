using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkeletonController : MonoBehaviour
{
    enum SkeletonState
    {
        idle,
        patrol,
        aggressed,
        attack,
        stagger,
        death
    }

    public float speed = 1.0f;
    public float runSpeed = 2.0f;
    public float health = 4.0f;
    public float approachThreshold = 0.1f;
    public float minIdletime = 1.0f;
    public float maxIdleTime = 2.0f;
    public float attackRange = 1.5f;
    public float attackInterval = 1.0f;
    public float staggerTime = 0.4f;
    public float maxStaggerDistance = 2.0f;
    public float staggerSpeed = 1.0f;
    public float aggressionRange = 6;
    public BoxCollider2D aggressionTrigger;
    public List<Transform> patrolWaypoint;
    public UnityEvent OnDeath;
    float idleTimer;
    float attackTimer;
    float staggerTimer;
    int waypointCounter;
    Vector3 staggerPosition;
    Transform player;
    [SerializeField]
    SkeletonState state = SkeletonState.idle;
    Animator animator;
    SpriteRenderer sprite;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        aggressionTrigger.size = new Vector2(aggressionRange, 2);
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case SkeletonState.idle:
                UpdateIdleState();
                break;
            case SkeletonState.patrol:
                UpdatePatrolState();
                break;
            case SkeletonState.aggressed:
                UpdateAggressedState();
                break;
            case SkeletonState.attack:
                UpdateAttackState();
                break;
            case SkeletonState.stagger:
                UpdateStaggerState();
                break;
            case SkeletonState.death:
                UpdateDeathState();
                break;
        }
    }

    void UpdateIdleState()
    {
        if (player)
        {
            TransitionToAggressed();
            return;
        }
        idleTimer -= Time.deltaTime;
        if (idleTimer <= 0)
        {
            TransitionToPatrol();
        }
    }
    void UpdatePatrolState()
    {
        if (player)
        {
            animator.SetBool("isWalking", false);
            TransitionToAggressed();
            return;
        }
        Vector3 currentTarget = patrolWaypoint[waypointCounter].position;
        Vector2 moveVector = ((currentTarget - transform.position).x * Vector2.right).normalized;
        transform.Translate(moveVector * speed * Time.deltaTime);
        float targetDistance = (currentTarget - transform.position).magnitude;
        if (targetDistance <= approachThreshold)
        {
            animator.SetBool("isWalking", false);
            TransitionToIdle();
        }
        else
        {
            sprite.flipX = (moveVector.x < 0) ? true : false;
        }
    }
    void UpdateAggressedState()
    {
        if (!player)
        {
            animator.SetBool("isRunning", false);
            TransitionToIdle();
            return;
        }
        Vector2 moveVector = ((player.position - transform.position).x * Vector2.right).normalized;
        transform.Translate(moveVector * runSpeed * Time.deltaTime);
        float targetDistance = (player.position - transform.position).magnitude;
        if (targetDistance <= attackRange)
        {
            animator.SetBool("isRunning", false);
            TransitionToAttack();
        }
        else
        {
            sprite.flipX = (moveVector.x < 0) ? true : false;
        }
    }
    void UpdateAttackState()
    {
        if (!player)
        {
            TransitionToIdle();
            return;
        }
        attackTimer += Time.deltaTime;
        if (attackTimer >= attackInterval)
        {
            animator.SetTrigger("Attack");
            attackTimer = 0;
        }
        float targetDistance = (player.position - transform.position).magnitude;
        if (targetDistance > attackRange)
        {
            TransitionToAggressed();
        }
        else
        {
            sprite.flipX = ((player.position - transform.position).x < 0) ? true : false;
        }
    }
    void UpdateStaggerState()
    {
        if (!player)
        {
            TransitionToIdle();
            return;
        }
        staggerTimer += Time.deltaTime;
        Vector3 moveVector = ((transform.position - player.position).x * Vector3.right).normalized;
        transform.position = Vector3.Lerp(transform.position, staggerPosition, staggerSpeed * Time.deltaTime);
        if (staggerTimer >= staggerTime)
        {
            TransitionToAggressed();
        }
    }
    void UpdateDeathState()
    {
    }
    public void OnBeingHit(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            state = SkeletonState.death;
            animator.SetTrigger("Killed");
            GetComponent<Collider2D>().enabled = false;
            this.enabled = false;
        }
        else
        {
            state = SkeletonState.stagger;
            animator.SetTrigger("Hit");
            Vector3 targetVector = player.position - transform.position;
            staggerPosition = transform.position - (targetVector.x * Vector3.right).normalized * (maxStaggerDistance / targetVector.magnitude);
            staggerTimer = 0;
        }
    }

    void InvokeWinEvent()
    {
        OnDeath.Invoke();
        Time.timeScale = 0f;
    }

    void TransitionToIdle()
    {
        idleTimer = Random.Range(minIdletime, maxIdleTime);
        state = SkeletonState.idle;
    }
    void TransitionToPatrol()
    {
        waypointCounter = Random.Range(0, 3);
        animator.SetBool("isWalking", true);
        state = SkeletonState.patrol;
    }
    void TransitionToAggressed()
    {
        animator.SetBool("isRunning", true);
        state = SkeletonState.aggressed;
    }
    void TransitionToAttack()
    {
        attackTimer = attackInterval;
        state = SkeletonState.attack;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            player = collision.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            player = null;
        }
    }
}
