using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    enum PlayerState
    {
        ground,
        rise,
        fall,
        attack
    }

    //substates of ground state
    public float speed = 3;
    public float jumpUpSpeed = 3;
    public float gravity = 9.8f;
    public float groundingPointOffset = .01f;
    public float groundDetectorRadius = .3f;
    public Vector3 groundDetectorOffset = Vector3.zero;
    public float collisionDetectorRadius = .1f;
    public Vector3 collisionDetectorOffsetL = Vector3.zero;
    public Vector3 collisionDetectorOffsetR = Vector3.zero;
    public LayerMask terrain;
    public LayerMask obstacale;
    public List<Collider2D> hitBoxes;

    [SerializeField]
    PlayerState state = PlayerState.fall;
    [SerializeField]
    int moveDirection = 0;
    [SerializeField]
    float horizontalSpeed = 0;
    [SerializeField]
    float verticalSpeed = 0;
    [SerializeField]
    bool attackFinished;

    RaycastHit2D groundDetectHit;
    SpriteRenderer sprite;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 moveVector = Vector2.zero;
        verticalSpeed -= gravity * Time.deltaTime;
        switch (state)
        {
            case PlayerState.ground:
                UpdateGroundState();
                break;
            case PlayerState.rise:
                UpdateRiseState();
                break;
            case PlayerState.fall:
                UpdateFallState();
                break;
            case PlayerState.attack:
                UpdateAttackState();
                break;
            default:
                break;
        }
        moveVector.x = horizontalSpeed * Time.deltaTime;
        moveVector.y = verticalSpeed * Time.deltaTime;
        if (HitLeftWall() || HitRightWall())
        {
            moveVector.x = 0;
        }
        transform.Translate(moveVector);

        if (moveVector.x == 0)
        {
            animator.SetBool("isRunning", false);
        }
        else
        {
            animator.SetBool("isRunning", true);
        }
    }

    void UpdateGroundState()
    {
        //make sure the character stands on the ground
        groundDetectHit = Physics2D.CircleCast(transform.position + groundDetectorOffset, groundDetectorRadius, -Vector2.up, 0, terrain);
        if (groundDetectHit.collider)
        {
            //set current height to raycast hit point, so I won't fall through when going up hills
            Vector2 currentPos = transform.position;
            currentPos.y = groundDetectHit.point.y - groundingPointOffset; //need to set character a little lower than the contact point to ensure solid grounding
            transform.position = currentPos;
            verticalSpeed = 0;
        }
        else
        {
            state = PlayerState.fall;
            animator.SetBool("isFalling", true);
            return;
        }

        if (Input.GetKey(KeyCode.A))
        {
            MoveLeft();
        }

        if (Input.GetKey(KeyCode.D))
        {
            MoveRight();
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            StopLeft();
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            StopRight();
        }

        //these two action leads to state transition, process only one of them
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
            return;
        }
        else if (Input.GetKeyDown(KeyCode.J))
        {
            moveDirection = 0;
            Attack();
        }

        horizontalSpeed = moveDirection * speed;
    }

    void UpdateRiseState()
    {
        if (verticalSpeed < 0)
        {
            state = PlayerState.fall;
            animator.SetBool("isFalling", true);
        }
    }

    void UpdateFallState()
    {
        groundDetectHit = Physics2D.CircleCast(transform.position + groundDetectorOffset, groundDetectorRadius, -Vector2.up, 0, terrain);
        if (groundDetectHit.collider)
        {
            state = PlayerState.ground;
            moveDirection = 0;
            animator.SetBool("isFalling", false);
        }
    }

    void UpdateAttackState()
    {
        //make sure the character stands on the ground
        groundDetectHit = Physics2D.CircleCast(transform.position + groundDetectorOffset, groundDetectorRadius, -Vector2.up, 0, terrain);
        if (groundDetectHit.collider)
        {
            //set current height to raycast hit point, so I won't fall through when going up hills
            Vector2 currentPos = transform.position;
            currentPos.y = groundDetectHit.point.y - groundingPointOffset; //need to set character a little lower than the contact point to ensure solid grounding
            transform.position = currentPos;
            verticalSpeed = 0;
        }
        else
        {
            state = PlayerState.fall;
            animator.SetBool("isFalling", true);
            return;
        }

        //attack continuously if holding attack key
        if (Input.GetKey(KeyCode.J))
        {
            Attack();
            return;
        }
        if (Input.GetKeyUp(KeyCode.J))
        {
            attackFinished = true;//let animation complete before state transition
        }
    }

    //this method is being call by the animation event which triggers at the end of attack animation
    void AttackFinished()
    {
        if (attackFinished)
        {
            state = PlayerState.ground;
            animator.SetBool("isAttacking", false);
        }
    }

    void MoveLeft()
    {
        sprite.flipX = true;
        moveDirection = -1;
    }

    void StopLeft()
    {
        if (moveDirection == -1)
            moveDirection -= -1;
    }

    void MoveRight()
    {
        sprite.flipX = false;
        moveDirection = 1;
    }

    void StopRight()
    {
        if (moveDirection == 1)
            moveDirection -= 1;
    }

    void Jump()
    {
        state = PlayerState.rise;
        verticalSpeed += jumpUpSpeed;
        animator.SetTrigger("Jump");
    }

    void Attack()
    {
        animator.SetBool("isAttacking", true);
        attackFinished = false;
        state = PlayerState.attack;
    }

    bool HitLeftWall()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position + collisionDetectorOffsetL, collisionDetectorRadius, -Vector2.up, 0, terrain | obstacale);
        return hit.collider && (moveDirection == -1);
    }
    bool HitRightWall()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position + collisionDetectorOffsetR, collisionDetectorRadius, -Vector2.up, 0, terrain | obstacale);
        return hit.collider && (moveDirection == 1);
    }

    void EnableHitBox(int index)
    {
        hitBoxes[index].enabled = true;
    }

    void DisableHitBox(int index)
    {
        hitBoxes[index].enabled = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position + groundDetectorOffset, groundDetectorRadius);
        Gizmos.DrawWireSphere(transform.position + collisionDetectorOffsetL, collisionDetectorRadius);
        Gizmos.DrawWireSphere(transform.position + collisionDetectorOffsetR, collisionDetectorRadius);
    }

}
